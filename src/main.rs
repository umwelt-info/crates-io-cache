use std::env::var;
use std::error::Error as StdError;
use std::fs::{remove_dir, remove_file};
use std::io::Error as IoError;
use std::mem::take;
use std::net::SocketAddr;
use std::path::Path;
use std::sync::atomic::{AtomicUsize, Ordering};
use std::time::{Duration, SystemTime};

use futures_util::stream::try_unfold;
use http::uri::{Authority, Scheme};
use hyper::{
    body::{Body, HttpBody},
    client::HttpConnector,
    header::{CONTENT_LENGTH, CONTENT_TYPE, LOCATION},
    service::service_fn,
    Client, Method, Request, Response, Server, StatusCode, Uri,
};
use hyper_rustls::{HttpsConnector, HttpsConnectorBuilder};
use tokio::{
    fs::{create_dir_all, metadata, remove_file as remove_file_async, rename, write, File},
    io::{AsyncWriteExt, BufWriter},
    process::Command,
    task::{spawn, spawn_blocking},
    time::{interval, MissedTickBehavior},
};
use tower::make::Shared;
use tower_http::services::ServeDir;
use walkdir::WalkDir;

#[tokio::main]
async fn main() -> Fallible {
    let bind_addr = var("BIND_ADDR")?.parse::<SocketAddr>()?;

    if metadata("index").await.is_err() {
        clone_index(bind_addr.port()).await?;
    }

    spawn(update_index());

    spawn(remove_old_crates());

    let https = HttpsConnectorBuilder::new()
        .with_native_roots()
        .https_only()
        .enable_http1()
        .enable_http2()
        .build();

    let state = &*Box::leak(Box::new(State {
        client: Client::builder().build(https),
        part_gen: AtomicUsize::new(0),
    }));

    let fallback_service = service_fn(move |req| async move {
        let resp = match download_crate(state, req).await {
            Ok(Some(resp)) => resp,
            Ok(None) => Response::builder()
                .status(StatusCode::NOT_FOUND)
                .body(Body::empty())
                .unwrap(),
            Err(err) => {
                let msg = format!("Failed to download crate: {}", err);

                eprintln!("{}", msg);

                Response::builder()
                    .status(StatusCode::INTERNAL_SERVER_ERROR)
                    .header(CONTENT_TYPE, "text/plain")
                    .body(msg.into())
                    .unwrap()
            }
        };

        Ok::<_, IoError>(resp)
    });

    let service = Shared::new(ServeDir::new(".").fallback(fallback_service));

    Server::bind(&bind_addr).serve(service).await?;

    Ok(())
}

async fn clone_index(port: u16) -> Fallible {
    let status = Command::new("git")
        .args([
            "clone",
            "https://github.com/rust-lang/crates.io-index.git",
            "index",
        ])
        .status()
        .await?;
    if !status.success() {
        return Err("Failed to clone crates.io index".into());
    }

    let output = Command::new("hostname").output().await?;
    if !output.status.success() {
        return Err("Failed to determine hostname".into());
    }

    let mut hostname = String::from_utf8(output.stdout)?;
    hostname.pop();

    write(
        "index/config.json",
        format!(
            r#"{{
"dl": "http://{hostname}:{port}/api/v1/crates",
"api": "https://crates.io"
}}"#
        ),
    )
    .await?;

    let status = Command::new("git")
        .args(["config", "user.name", "crates-io-cache"])
        .current_dir("index")
        .status()
        .await?;
    if !status.success() {
        return Err("Failed to set user name config".into());
    }

    let status = Command::new("git")
        .args(["config", "user.email"])
        .arg(format!("crates-io-cache@{hostname}"))
        .current_dir("index")
        .status()
        .await?;
    if !status.success() {
        return Err("Failed to set user email config".into());
    }

    let status = Command::new("git")
        .args(["commit", "--all", "--message", "Override config"])
        .current_dir("index")
        .status()
        .await?;
    if !status.success() {
        return Err("Failed to commit config override".into());
    }

    let status = Command::new("git")
        .arg("update-server-info")
        .current_dir("index")
        .status()
        .await?;
    if !status.success() {
        return Err("Failed to update server info".into());
    }

    Ok(())
}

async fn update_index() -> Fallible {
    let mut interval = interval(Duration::from_secs(59 * 60));
    interval.set_missed_tick_behavior(MissedTickBehavior::Skip);

    loop {
        interval.tick().await;

        let status = Command::new("git")
            .args(["pull", "--rebase"])
            .current_dir("index")
            .status()
            .await?;
        if !status.success() {
            eprintln!("Failed to pull updated index");
            continue;
        }

        let status = Command::new("git")
            .arg("update-server-info")
            .current_dir("index")
            .status()
            .await?;
        if !status.success() {
            eprintln!("Failed to update server info");
            continue;
        }
    }
}

async fn remove_old_crates() -> Fallible {
    let mut interval = interval(Duration::from_secs(61 * 60));
    interval.set_missed_tick_behavior(MissedTickBehavior::Skip);

    loop {
        interval.tick().await;

        fn inner() -> Fallible {
            let deadline = SystemTime::now() - Duration::from_secs(7 * 24 * 60 * 60);

            for entry in WalkDir::new("api").contents_first(true) {
                let entry = entry?;

                if entry.file_type().is_file() {
                    if entry.metadata()?.accessed()? < deadline {
                        println!("Removing old crate {}", entry.path().display());
                        remove_file(entry.path())?;
                    }
                } else if entry.file_type().is_dir() {
                    let _ = remove_dir(entry.path());
                }
            }

            Ok(())
        }

        if let Err(err) = spawn_blocking(inner).await.unwrap() {
            eprintln!("Failed to remove old crates: {err}");
        }
    }
}

async fn download_crate(
    state: &'static State,
    mut req: Request<Body>,
) -> Fallible<Option<Response<Body>>> {
    if req.method() != Method::GET {
        return Ok(None);
    }

    let path = req.uri().path();

    let Some(name) = path
        .strip_prefix("/api/v1/crates/")
        .and_then(|path| path.strip_suffix("/download")) else { return Ok(None) };

    println!("Downloading: {name}");

    let mut path = Path::new(path).strip_prefix("/").unwrap().to_owned();

    let part_gen = state.part_gen.fetch_add(1, Ordering::Relaxed);
    path.set_extension(format!("part-{part_gen}"));

    let mut parts = take(req.uri_mut()).into_parts();

    parts.scheme = Some(Scheme::HTTPS);
    parts.authority = Some(Authority::from_static("crates.io"));

    *req.uri_mut() = Uri::from_parts(parts).unwrap();

    let mut resp = state.client.request(req).await?;

    if resp.status() == StatusCode::FOUND {
        if let Some(location) = resp.headers().get(LOCATION) {
            let uri = Uri::try_from(location.as_bytes())?;

            resp = state.client.get(uri).await?;
        } else {
            return Ok(Some(resp));
        }
    }

    if !resp.status().is_success() {
        return Ok(Some(resp));
    }

    let size = match resp.headers().get(CONTENT_LENGTH) {
        Some(content_length) => content_length.to_str()?.parse()?,
        None => 0,
    };

    create_dir_all(path.parent().unwrap()).await?;

    let file = File::create(&path).await?;
    file.set_len(size).await?;

    struct Context {
        path: std::path::PathBuf,
        writer: BufWriter<File>,
        body: Body,
        ok: bool,
    }

    impl Drop for Context {
        fn drop(&mut self) {
            if self.ok {
                return;
            }

            let path = take(&mut self.path);

            spawn(async move {
                eprintln!("Removing incomplete download: {}", path.display());
                let _ = remove_file_async(path).await;
            });
        }
    }

    let ctx = Context {
        path,
        writer: BufWriter::with_capacity(BUF_CAP, file),
        body: resp.into_body(),
        ok: false,
    };

    let body = Body::wrap_stream::<_, _, Error>(try_unfold(ctx, move |mut ctx| async {
        match ctx.body.data().await {
            None => {
                ctx.writer.flush().await?;

                let from = take(&mut ctx.path);
                let mut to = from.clone();
                to.set_extension("");

                rename(from, to).await?;

                ctx.ok = true;

                Ok(None)
            }
            Some(Ok(buf)) => {
                ctx.writer.write_all(&buf).await?;

                Ok(Some((buf, ctx)))
            }
            Some(Err(err)) => Err(err.into()),
        }
    }));

    let resp = Response::builder()
        .header(CONTENT_TYPE, "application/octet-stream")
        .body(body)
        .unwrap();

    Ok(Some(resp))
}

struct State {
    client: Client<HttpsConnector<HttpConnector>>,
    part_gen: AtomicUsize,
}

const BUF_CAP: usize = 64 * 1024;

type Fallible<T = ()> = Result<T, Error>;

type Error = Box<dyn StdError + Send + Sync>;
